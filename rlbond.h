INL
bits32_t msb32iso_rlbond(REG bits32_t bits) /* branchless isolate */
{
    bits |= (bits >> 0x01);
    bits |= (bits >> 0x02);
    bits |= (bits >> 0x04);
    bits |= (bits >> 0x08);
    bits |= (bits >> 0x10);
    return bits & ~(bits >> 1);
}

INL
bitidx_t msb32idx_rlbond(REG bits32_t bits) /* branchless isolate + de Bruijn */
{
    bits |= (bits >> 0x01);
    bits |= (bits >> 0x02);
    bits |= (bits >> 0x04);
    bits |= (bits >> 0x08);
    bits |= (bits >> 0x10);
    bits = bits & ~(bits >> 1);
    return (bitidx_t) ( DeBruijnLUT32[(uint32_t)(bits * DeBruijnKey32) >> 27]
                        | (!bits * sizeof(bits) * 8) );
}
