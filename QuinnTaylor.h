INL
bitidx_t msb32idx_QuinnTaylor(REG bits32_t bits) /* looped compute */
{
    if (!bits) return sizeof(bits) * 8;

    bitidx_t r = 0;

    while (bits >>= 1)
        r++;

    return r;
}
