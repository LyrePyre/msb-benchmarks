/* nearly identical to rlbond.. */
/* ... rlbond posted first too. */

INL
bits32_t msb32iso_Protagonist(REG bits32_t bits) /* branchless isolate */
{
    bits |= bits >> 0x01;
    bits |= bits >> 0x02;
    bits |= bits >> 0x04;
    bits |= bits >> 0x08;
    bits |= bits >> 0x10;
    return (bits >> 0x01) + !!bits;
}

INL
bitidx_t msb32idx_Protagonist(REG bits32_t bits) /* branchless isolate + de Bruijn */
{
    bits |= bits >> 0x01;
    bits |= bits >> 0x02;
    bits |= bits >> 0x04;
    bits |= bits >> 0x08;
    bits |= bits >> 0x10;
    bits = (bits >> 0x01) + !!bits;
    return (bitidx_t) ( DeBruijnLUT32[(uint32_t)(bits * DeBruijnKey32) >> 27]
                        | (!bits * sizeof(bits) * 8) );
}
