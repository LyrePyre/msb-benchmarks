INL
bitidx_t msb32idx_user3177100(REG bits32_t bits) /* successive approximation */
{
    if (!bits) return sizeof(bits) * 8;

    bitidx_t msb  = sizeof(bits) * 4;
    bitidx_t step = msb;
    while (step > 1) {
        step /= 2;
        if (bits >> msb)
            msb += step;
        else
            msb -= step;
    }
    if (bits >> msb)
        msb++;
    return msb - 1;
}
