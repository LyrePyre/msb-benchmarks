/*! ***************************************************************************
@file       printbin.h
@author     Levi Perez (levi\@leviperez.dev)
@date       2024-04-26
@brief
    Code to format binary representations of integers as strings.
******************************************************************************/

#pragma once
#ifndef PRINTBIN_H_
#define PRINTBIN_H_

#ifndef EOF
#include <stdio.h>
#endif

#define PRINTBIN_FORMAT8 "%c%c%c%c%c%c%c%c"
#define PRINTBIN_ARGS_FROM_BYTE(byte) \
        ('0' + !!(byte & 0x80)), \
        ('0' + !!(byte & 0x40)), \
        ('0' + !!(byte & 0x20)), \
        ('0' + !!(byte & 0x10)), \
        ('0' + !!(byte & 0x08)), \
        ('0' + !!(byte & 0x04)), \
        ('0' + !!(byte & 0x02)), \
        ('0' + !!(byte & 0x01))

#ifdef __cplusplus
extern "C" {
#endif

char const* printbin8(uint8_t bits) /* WARN: Not thread safe. */
{
    static char buffer[sizeof(uint8_t) * 9] = { 0 };
    sprintf(buffer, PRINTBIN_FORMAT8,
                    PRINTBIN_ARGS_FROM_BYTE(bits));
    return buffer;
}

char const* printbin16(uint16_t bits) /* WARN: Not thread safe. */
{
    static char buffer[sizeof(uint16_t) * 9] = { 0 };
    sprintf(buffer, PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8,
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x08),
                    PRINTBIN_ARGS_FROM_BYTE(bits));
    return buffer;
}

char const* printbin32(uint32_t bits) /* WARN: Not thread safe. */
{
    static char buffer[sizeof(uint32_t) * 9] = { 0 };
    sprintf(buffer, PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8,
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x18),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x10),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x08),
                    PRINTBIN_ARGS_FROM_BYTE(bits));
    return buffer;
}

char const* printbin64(uint64_t bits) /* WARN: Not thread safe. */
{
    static char buffer[sizeof(uint64_t) * 9] = { 0 };
    sprintf(buffer, PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8" "
                    PRINTBIN_FORMAT8,
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x38),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x30),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x28),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x20),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x18),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x10),
                    PRINTBIN_ARGS_FROM_BYTE(bits >> 0x08),
                    PRINTBIN_ARGS_FROM_BYTE(bits));
    return buffer;
}

#ifdef __cplusplus
} /* extern "C" */
#endif

#undef PRINTBIN_FORMAT8
#undef PRINTBIN_ARGS_FROM_BYTE

#endif /* ifndef PRINTBIN_H_ */
