/*! ***************************************************************************
@file       debruijn_luts.h
@author     Levi Perez (levi\@leviperez.dev)
@date       2024-04-24
@brief
    These are my own de Bruijn lookups + keys.  They differ from most found
    online, but work exactly the same, and at the same runtime cost.
@remarks
    I originally generated them for a fast and portable CTZ implementation,
    but it seems they work just the same for MSB-related shenanigans as LSB.
******************************************************************************/

#pragma once
#ifndef DEBRUIJN_LUTS_H_
#define DEBRUIJN_LUTS_H_

static const uint32_t DeBruijnLUT8[8] = {
    0, 1, 2, 4, 7, 3, 6, 5
};
#define DeBruijnKey8 0x17u;

static const uint32_t DeBruijnLUT16[16] = {
    0, 1, 2, 5, 3, 9, 6, 11,
    15, 4, 8, 10, 14, 7, 13, 12
};
#define DeBruijnKey16 0x9AFu;

static const uint32_t DeBruijnLUT32[32] = {
    0, 1, 2, 6, 3, 11, 7, 16, 4, 14,
    12, 21, 8, 23, 17, 26, 31, 5, 10,
    15, 13, 20, 22, 25, 30, 9, 19, 24,
    29, 18, 28, 27
};
#define DeBruijnKey32 0x4653ADFu

static const uint32_t DeBruijnLUT64[64] = {
    0, 1, 2, 7, 3, 13, 8, 19, 4, 25,
    14, 28, 9, 34, 20, 40, 5, 17, 26,
    38, 15, 46, 29, 48, 10, 31, 35,
    54, 21, 50, 41, 57, 63, 6, 12, 18,
    24, 27, 33, 39, 16, 37, 45, 47,
    30, 53, 49, 56, 62, 11, 23, 32,
    36, 44, 52, 55, 61, 22, 43, 51,
    60, 42, 59, 58
};
#define DeBruijnKey64 0x218A392CD3D5DBFuL

#endif /* ifndef DEBRUIJN_LUTS_H_ */
