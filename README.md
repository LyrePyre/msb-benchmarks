Doin some casual benchmarks for the solutions in this old Stack Overflow page:
- https://stackoverflow.com/questions/671815/what-is-the-fastest-most-efficient-way-to-find-the-highest-set-bit-msb-in-an-i

Not done going through all the top solutions / discussions, but what's here is pretty telling.

Am compiling and running on:
- Windows 10 w/ Intel x64 i7-8750H (2.2 GHz)
- Raspberry Pi 4b w/ ARM v8 Cortex-A72 (1.8 GHz)
(at the time of writing)
