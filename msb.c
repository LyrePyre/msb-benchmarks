/*! ***************************************************************************
@file       msb.c
@author     Levi Perez (levi\@leviperez.dev)
@date       2024-04-24
@brief
    Benchmarking driver for various Most-Significant Bit related implementations
    found online.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>

#ifndef RLEN_MULT
#define RLEN_MULT    1000000
#endif
#ifndef DEFAULT_RLEN
#define DEFAULT_RLEN 25
#endif

typedef uint32_t bitidx_t;
typedef uint64_t bits64_t;
typedef uint32_t bits32_t;
typedef uint16_t bits16_t;
typedef uint8_t  bits8_t;

#if defined(USE_REGISTER_PARAMS) && USE_REGISTER_PARAMS
    #define REG register
#else
    #define REG
#endif

#if defined(USE_INLINED_FUNCS) && USE_INLINED_FUNCS
    #define INL static inline
#else
    #define INL
#endif

#include "printbin.h"      /* utils for printing values in binary */
#include "debruijn_luts.h" /* de Bruijn lookup tables & keys */

#include "user3177100.h"
#include "dangermouse.h"
#include "VoidStar.h"
#include "rlbond.h"
#include "Protagonist.h"
#include "QuinnTaylor.h"
#include "timday.h"

/* Use an array of anonymous structs to define test entries: */
static struct {
    char const* user;
    char const* desc;
    struct {
        union {
        bitidx_t (*msb32idx) (bits32_t);
        bits32_t (*msb32iso) (bits32_t);
        } ptr;
        bool fail;
        uint32_t ops_per_sec;
    } fn[2];
}
Tests[] = {
    { "QuinnTaylor", "looped compute", {
        { {msb32idx_QuinnTaylor}, 0, 0 },
        { 0 }
    } },
    { "timday",      "inline assembly bitscan", {
        { {msb32idx_timday}, 0, 0 },
        { {msb32iso_timday}, 0, 0 }
    } },
    { "Protagonist", "branchless isolate + de Bruijn", {
        { {msb32idx_Protagonist}, 0, 0 },
        { {msb32iso_Protagonist}, 0, 0 }
    } },
    { "rlbond",      "branchless isolate + de Bruijn", {
        { {msb32idx_rlbond}, 0, 0 },
        { {msb32iso_rlbond}, 0, 0 }
    } },
    { "VoidStar",    "loopless compute", {
        { {msb32idx_VoidStar}, 0, 0 },
        { 0 }
    } },
    { "dangermouse", "bitmask binary search", {
        { {msb32idx_dangermouse}, 0, 0 },
        { 0 }
    } },
    { "user3177100", "successive approximation", {
        { {msb32idx_user3177100}, 0, 0 },
        { 0 }
    } },
};
static const size_t TestCount = sizeof(Tests) / sizeof(Tests[0]);


static
void print_info(const size_t rlen)
{
    printf("\n# INFO\n");
    printf("  * random inputs size: %g x %d\n", (float)rlen / RLEN_MULT, RLEN_MULT);
    printf("  * clock() ticks per second: %ld\n", CLOCKS_PER_SEC);
    #ifdef USE_FAST_TYPES
    /* Hi future self.  After testing, it seems fast types only created more problems, */
    /* and no real speed benefit on modern CPUs.  Thus, this flag is now ignored.      */
    printf("  * USE_FAST_TYPES: 0\n");
    #endif
    #ifdef USE_REGISTER_PARAMS
    printf("  * USE_REGISTER_PARAMS: %d\n", USE_REGISTER_PARAMS);
    #endif
    #ifdef USE_INLINED_FUNCS
    printf("  * USE_INLINED_FUNCS: %d\n", USE_INLINED_FUNCS);
    #endif

    putc('\n', stdout);
}

static
void test_correctness(void)
{
    size_t i;
    bitidx_t expectIdx = 31;

    do {
        bits32_t bits = 1u << expectIdx;
        bits |= rand() & (bits - 1);

        for (i = 0; i < TestCount; ++i)
        {
            bitidx_t (*msb32idx) (bits32_t) = Tests[i].fn[0].ptr.msb32idx;
            bits32_t (*msb32iso) (bits32_t) = Tests[i].fn[1].ptr.msb32iso;

            /* test the msb index impl (if applicable) */
            if (msb32idx)
            {
                bitidx_t actual = msb32idx(bits);
                Tests[i].fn[0].fail |= ( actual != expectIdx ||
                                         msb32idx(0) != sizeof(bits) * 8 );
            }

            /* test the isolate msb impl (if applicable) */
            if (msb32iso)
            {
                bits32_t actual = msb32iso(bits);
                Tests[i].fn[1].fail |= ( actual != (1u << expectIdx) ||
                                         msb32iso(0) != 0 );
            }
        }
    } while (expectIdx --> 0);

    printf("# Correctness Tests\n");

    for (i = 0; i < TestCount; ++i)
    {
        if (Tests[i].fn[0].ptr.msb32idx)
        {
            printf("%16s | MSB Index:     %s\n",
                   Tests[i].user,
                   Tests[i].fn[0].fail ? "FAIL" : "pass");
        }
        if (Tests[i].fn[1].ptr.msb32iso)
        {
            printf("%16s | MSB Isolation: %s\n",
                   Tests[i].user,
                   Tests[i].fn[1].fail ? "FAIL" : "pass");
        }
    }

    putc('\n', stdout);
}

static
bits32_t* init_random_data(const size_t rlen)
{
    size_t r;

    bits32_t* rands = (bits32_t*) calloc(sizeof(bits32_t), rlen);

    if (!rands)
    {
        fprintf(stderr, "ERR: rlen is too large on this machine; cannot alloc test memory."
                        "  (0x%zX)", rlen);
        exit(EXIT_FAILURE);
        return NULL;
    }

    r = rlen;
    while (r --> 0)
    {
        rands[r] = (bits32_t) rand();
    }

    return rands;
}

static
void fini_random_data(bits32_t** data)
{
    if (data && *data)
        free(*data);
    *data = NULL;
}

static
void benchmark_msb32(bits32_t const* data, const size_t rlen)
{
    size_t i;
    for (i = 0; i < TestCount; ++i)
    {
        bitidx_t (*msb32idx) (bits32_t) = Tests[i].fn[0].ptr.msb32idx;
        bits32_t (*msb32iso) (bits32_t) = Tests[i].fn[1].ptr.msb32iso;

        if (msb32idx)
        {
            size_t r = 0;
            const clock_t start = clock();
            clock_t now = start;

            do {
                volatile /* this fixes -O2 "optimizing" out our call payload */
                bitidx_t trash = msb32idx(data[r]);
                (void)   trash;

                now = clock();
                r = (r + 1) % rlen;
                ++ Tests[i].fn[0].ops_per_sec;
            } while (now < start + CLOCKS_PER_SEC);
        }

        if (msb32iso)
        {
            size_t r = 0;
            const clock_t start = clock();
            clock_t now = start;

            do {
                volatile /* this fixes -O2 "optimizing" out our call payload */
                bitidx_t trash = msb32iso(data[r]);
                (void)   trash;

                now = clock();
                r = (r + 1) % rlen;
                ++ Tests[i].fn[1].ops_per_sec;
            } while (now < start + CLOCKS_PER_SEC);
        }
    }

    printf("# Benchmarks: MSB Index (32-bit):\n");
    for (i = 0; i < TestCount; ++i)
    {
        if (!Tests[i].fn[0].ptr.msb32idx)
            continue;
        printf("%16s | %32s\t%u\n",
               Tests[i].user,
               Tests[i].desc,
               Tests[i].fn[0].ops_per_sec / 1000);
    }
    putc('\n', stdout);

    printf("# Benchmarks: MSB Isolation (32-bit):\n");
    for (i = 0; i < TestCount; ++i)
    {
        if (!Tests[i].fn[1].ptr.msb32iso)
            continue;
        printf("%16s | %32s\t%u\n",
               Tests[i].user,
               Tests[i].desc,
               Tests[i].fn[1].ops_per_sec / 1000);
    }
    putc('\n', stdout);
}

static
void dbg_print_msb32idx(bitidx_t (*fn) (bits32_t))
{
    bits32_t bits = 1u << 31, i = 31;
    do {
        bitidx_t qt = fn(bits);
        printf("%2u|%s|: %2u\n", i--, printbin32(bits), qt);
    } while (bits >>= 1);
    printf("%2u|%s|: %2u\n", (uint32_t)(sizeof(bits) * 8), printbin32(0), fn(0));
}


int main(int argc, char const* const argv[])
{
    const size_t rlen = (size_t)(RLEN_MULT * (argc == 1 ? DEFAULT_RLEN
                                                        : atof(argv[1])));
    if (rlen < 1 || rlen > INT_MAX)
    {
        fprintf(stderr, "ERR: Bad rlen value. (0x%zX)", rlen);
        return EXIT_FAILURE;
    }

    while (0) /* squelch unused function warning(s) */
    {
        dbg_print_msb32idx(NULL);
    }

    srand(time(NULL));

    print_info(rlen);

    test_correctness();

    bits32_t* rands = init_random_data(rlen);
    {
        benchmark_msb32(rands, rlen);
    }
    fini_random_data(&rands);

    return EXIT_SUCCESS;
}
