INL
bitidx_t msb32idx_VoidStar(REG bits32_t bits) /* loopless compute */
{
    if (!bits) return sizeof(bits32_t) * 8;

    bitidx_t k = 0;
    if (bits > 0xFFFF) { bits >>= 0x10; k  = 0x10; }
    if (bits > 0x00FF) { bits >>= 0x08; k |= 0x08; }
    if (bits > 0x000F) { bits >>= 0x04; k |= 0x04; }
    if (bits > 0x0003) { bits >>= 0x02; k |= 0x02; }
    return k | ( (bits & 2) >> 1 );
}

