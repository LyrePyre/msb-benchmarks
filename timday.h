INL
bits32_t msb32iso_timday(volatile REG bits32_t bits)
{
    bitidx_t msb;
    #if __x86_64 || __x86_64__
    __asm__("bsrl %1,%0" : "=r"(msb) : "r"(bits));
    #elif __aarch64__
    __asm__("clz %w1,%w0" : "=r"(msb) : "r"(bits));
    msb = sizeof(bits) * 8 - 1 - msb;
    #else
    msb = 0; /* TODO */
    #endif
    return !!bits << msb;
}

INL
bitidx_t msb32idx_timday(volatile REG bits32_t bits)
{
    bitidx_t msb;
    #if __x86_64 || __x86_64__
    __asm__("bsrl %1,%0" : "=r"(msb) : "r"(bits));
    #elif __aarch64__
    __asm__("clz %w1,%w0" : "=r"(msb) : "r"(bits));
    msb = sizeof(bits) * 8 - !!bits - msb;
    #else
    msb = 0; /* TODO */
    #endif
    return msb | (!bits * sizeof(bits) * 8);
}
