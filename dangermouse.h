INL
bitidx_t msb32idx_dangermouse(REG bits32_t bits) /* bitmask binary search */
{
    static const bits32_t maskv[] = { 0xFFFF, 0xFF, 0xF, 0x3, 0x1 };
    const bits32_t* mask = maskv;
    bitidx_t lo = 0,
             hi = sizeof(bits) * 8;

    if (!bits) return hi;

    do {
        bitidx_t mi = lo + (hi - lo) / 2; /* (lo + hi) >> 1; */

        if (bits >> mi)
            lo = mi;
        else if (bits & (*mask << lo))
            hi = mi;

        mask++;
    } while (lo < hi - 1);

    return lo;
}
