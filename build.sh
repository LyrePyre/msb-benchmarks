#!/bin/bash

set -eo pipefail
IFS=$'\n'

SRCS=('msb.c')
EXE="${EXE:-run-benchmark}"
CC=${CC:-clang}
CSTD=${CSTD:-gnu99}
OPT=${OPT:-O0}
CFLAGS=('-Wall' '-Wextra' '-pedantic')
USEREG=${USEREG:-0}
USEINLINE=${USEINLINE:-0}
RUN=${RUN:-0}

while [[ $# -gt 0 ]]; do
    case "$1" in
        debug|-d|--debug)
            CFLAGS+=('-g' '-fno-omit-frame-pointer' '-DDEBUG=1') ;;
        *gcc|*clang)
            CC="$1" ;;
        c??|gnu??)
            CSTD="$1" ;;
        [Oo]*)
            OPT=${1^} ;;
        save|temps|save-temps)
            CFLAGS+=('-save-temps' '-masm=intel' '-Wno-gnu-line-marker') ;;
        usereg|reg)
            USEREG=1 ;;
        useinline|inline|inl)
            USEINLINE=1 ;;
        run)
            RUN=1 ;;
        *.[cis])
            SRCS+=("$1") ;;
        *)
            CFLAGS+=("$1") ;;
    esac
    shift
done

CFLAGS+=("-DUSE_REGISTER_PARAMS=$USEREG" "-DUSE_INLINED_FUNCS=$USEINLINE")

if [[ -n "$EXE" ]]; then
    EXE=('-o' "$EXE")
fi

echo \
"$CC" -std=$CSTD -$OPT ${CFLAGS[@]} ${SRCS[@]} ${EXE[@]}
"$CC" -std=$CSTD -$OPT ${CFLAGS[@]} ${SRCS[@]} ${EXE[@]}

if [[ $RUN -ne 0 && -n "$EXE" && -x "${EXE[1]}" ]]; then
    "./${EXE[1]}"
fi

